// function addStudent(){
//     var data = {
//         stdid : parseInt(document.getElementById("sid").value),
//         fname : document.getElementById("fname").value,
//         lname : document.getElementById("lname").value,
//         email : document.getElementById("email").value
//     }

//     //  fetch takes two argument, the url and the request object
//     //  need to specify the method explicitly otherwise, fetch defaults to GET requests.
//     var sid = data.stdid

//     fetch('/student', { // mention only the route defined in backend
//         method: "POST", // the method used to write the data
//         body: JSON.stringify(data), //convert the js object to json format using stringify from JSON library
//         headers: {"Content-type": "application/json; charset=UTF-8"} //request body
//         }); TouchEvent(response1 => { //response1 is the http respopnse 
//             if (response1.ok) { //checking response successs or not
//                 fetch("/student/" + sid) //only one parameter , no mehtod or data to send so no second parameter and default method taken is GET
//                 .then(response2 => response2.text()) // extract the data from response body, not all the response object 
//                 .then(data => showStudent(data))
//             }
//         })
// }

// //  Display the single student data in a table

// // function showStudent(data) {
// //     const student = JSON.parse(data) // converting JSON oject to js object using parse() method
// //     // Find a <table> element with id="myTable":
// //     var table = document.getElementById("myTable");

// //     // Create an empty <tr> element and add to the last position of the table:
// //     // table.length is the length of the table and iindex for the row .. dynamic row
// //     var row = table.insertRow(table.length);

// //     // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
// //     var td=[]
// //     for(i=0; i<table.rows[0].cells.length; i++){
// //     td[i] = row.insertCell(i); // adding a td tag <td/> or ceels in the table 
// //     } 

// // // Add student detail to the new cells:
// // td[0].innerHTML = student.stdid;
// // td[1].innerHTML = student.fname;
// // td[2].innerHTML = student.lname;
// // td[3].innerHTML = student.email;
// // td[4].innerHTML = '<input type="button" onclick="deleteStudent(this)"value="delete" id="button-1">'
// // td[5].innerHTML = '<input type="button" onclick="updateStudent(this)"value="edit" id="button-2">'
// // }

// function showStudent(data) {
//     const student = JSON.parse(data)
//     // Find a <table> element with id="myTable":
//     var table = document.getElementById("myTable");
//     // Create an empty <tr> element and add to the last position of the table:
//     var row = table.insertRow(table.length);
//     // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
//     var td=[]
//     for(i=0; i<table.rows[0].cells.length; i++){
//         td[i] = row.insertCell(i);
//     }
//     // Add student detail to the new cells:
//     td[0].innerHTML = student.stdid;
//     td[1].innerHTML = student.fname;
//     td[2].innerHTML = student.lname;
//     td[3].innerHTML = student.email;
//     td[4].innerHTML = '<input type="button" onclick="deleteStudent(this)"value="delete" id="button-1">';
//     td[5].innerHTML = '<input type="button" onclick="updateStudent(this)"value="edit" id="button-2">';
// }
window.onload = function() {
    fetch('/students')
    .then(response => response.text())
    .then(data => showStudents(data))
}



function addStudent(){
    var data = getFormData()
    // {
    // stdid : parseInt(document.getElementById("sid").value),
    // fname : document.getElementById("fname").value,
    // lname : document.getElementById("lname").value,
    // email : document.getElementById("email").value
    // }

    //  fetch takes two argument, the url and the request object
    //  need to specify the method explicitly otherwise, fetch defaults to GET requests.
    var sid = data.stdid

    fetch('/student', {  // mention only the route defined in backend
    method: "POST", // the method used to write the data
    body: JSON.stringify(data), //convert the js object to json format using stringify from JSON library
    headers: {"Content-type": "application/json; charset=UTF-8"}//request body
    }).then(response1 =>{ //checking response successs or not
        if(response1.ok){
            fetch('/student/'+sid)
            .then(response2 =>response2.text())
            .then(data=>showStudent(data))
        } else {
            //  catching error 
            throw new Error(response1.statusText)
        }
    }).catch(e => {
        alert(e)
    })

    if (isNaN(sid)) {
        alert("Enter valid student ID")
        return
        } else if (data.email == "") {
        alert("Email cannot be empty")
    } else if(data.fname == ""){
        alert("first name cannot be empty")
        return
        }

        resetForm()
}

function showStudent(data) {
    const student = JSON.parse(data)
    newRow(student)

    // Find a <table> element with id="myTable":
    // var table = document.getElementById("myTable");
    // // Create an empty <tr> element and add to the last position of the table:
    // var row = table.insertRow(table.length);
    // // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    // var td=[]
    // for(i=0; i<table.rows[0].cells.length; i++){
    //     td[i] = row.insertCell(i);
    // }
    // // Add student detail to the new cells:
    // td[0].innerHTML = student.stdid;
    // td[1].innerHTML = student.fname;
    // td[2].innerHTML = student.lname;
    // td[3].innerHTML = student.email;
    // td[4].innerHTML = '<input type="button" onclick="deleteStudent(this)"value="delete" id="button-1">';
    // td[5].innerHTML = '<input type="button" onclick="updateStudent(this)"value="edit" id="button-2">';
}


// set the form empty
function resetForm() {
    document.getElementById("sid").value = "";
    document.getElementById("fname").value = "";
    document.getElementById("lname").value = "";
    document.getElementById("email").value = "";
}


function showStudents(data) {
    const students = JSON.parse(data)
    students.forEach(stud => {
        newRow(stud)
    });
    // students.forEach(stud =>{
    //     var table = document.getElementById("myTable");
    //     var row = table.insertRow(table.length);
    //     var td=[]
    //     for(i=0; i<table.rows[0].cells.length; i++){
    //         td[i] = row.insertCell(i);
    //     }
    //     // Add student detail to the new cells:
    //     td[0].innerHTML = stud.stdid;
    //     td[1].innerHTML = stud.fname;
    //     td[2].innerHTML = stud.lname;
    //     td[3].innerHTML = stud.email;
    //     td[4].innerHTML = '<input type="button" onclick="deleteStudent(this)"value="delete" id="button-1">';
    //     td[5].innerHTML = '<input type="button" onclick="updateStudent(this)"value="edit" id="button-2">';
    // });
}

function newRow(student) {
    // Find a <table> element with id="myTable":
    var table = document.getElementById("myTable");
    // Create an empty <tr> element and add to the last position of the table:
    var row = table.insertRow(table.length);
    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var td=[]
    for(i=0; i<table.rows[0].cells.length; i++){
        td[i] = row.insertCell(i);
    }
     // Add student detail to the new cells:
     td[0].innerHTML = student.stdid;
     td[1].innerHTML = student.fname;
     td[2].innerHTML = student.lname;
     td[3].innerHTML = student.email;
     td[4].innerHTML = '<input type="button" onclick="deleteStudent(this)"value="delete" id="button-1">';
     td[5].innerHTML = '<input type="button" onclick="updateStudent(this)"value="edit" id="button-2">';
}

var selectedRow = null
function updateStudent(r) {
    // extracting the row 
    selectedRow = r.parentElement.parentElement;

    // fill in the form fields with selected row
    document.getElementById("sid").value = selectedRow.cells[0].innerHTML; //innerHTML to access the value to cell 
    document.getElementById("fname").value = selectedRow.cells[1].innerHTML;
    document.getElementById("lname").value = selectedRow.cells[2].innerHTML;
    document.getElementById("email").value = selectedRow.cells[3].innerHTML;

    var btn = document.getElementById("button-add");
    sid = selectedRow.cells[0].innerHTML;
    if (btn) {
        btn.innerHTML = "update";
        btn.setAttribute("onclick", "update(sid)");
    }
}

function update(sid) {
    var newData = getFormData()

    fetch('/student/' + sid, {  // mention only the route defined in backend
            method: "PUT", // the method used to write the data
            body: JSON.stringify(newData), //convert the js object to json format using stringify from JSON library
            headers: {"Content-type": "application/json; charset=UTF-8"}//r
    }).then(res => {
        if (res.ok) {
            selectedRow.cells[0].innerHTML = newData.stdid;
            selectedRow.cells[1].innerHTML = newData.fname;
            selectedRow.cells[2].innerHTML = newData.lname;
            selectedRow.cells[3].innerHTML = newData.email;

            // set the initial values
            var btn = getElementById("button-add");
            if (btn) {
                btn.innerHTML = "Add";
                btn.setAttribute("onclick", "addStudent()");
                selectedRow = null;

                resetForm()
            } else {
                alert ("Server: Update request error")
            } 
         }
    })
  
}

function getFormData() {
    var formData = {
        stdid : parseInt(document.getElementById("sid").value),
        fname : document.getElementById("fname").value,
        lname : document.getElementById("lname").value,
        email : document.getElementById("email").value
        }
        return formData
}

function deleteStudent(r){
    // this(input) -> td -> tr 
    if (confirm('Are you sure you want to DELETE this?')){
    selectedRow = r.parentElement.parentElement;
    sid = selectedRow.cells[0].innerHTML;

    fetch('/student/'+sid, {
        method: "DELETE",
        headers: {"Content-type": "application/json; charset=UTF-8"}
    });
    var rowIndex = selectedRow.rowIndex; // index starts from 0
    if (rowIndex>0) { //th is row 0
        document.getElementById("myTable").deleteRow(rowIndex);
    }
        selectedRow = null;
    }
}

