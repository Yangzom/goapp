package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent(w http.ResponseWriter, r *http.Request) {
	// cookie verification
	if !VerifyCookie(w, r){
		return
	}
	
	// fmt.Fprintf(w, "from add student handler")
	var stud model.Student
	decoder := json.NewDecoder(r.Body) //takes the request body as the parameter and returns a new decoder object
	err := decoder.Decode(&stud) // stores the decoded json values in the stud
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		// res_map := map[string]string{"error": "invalid json data"}
		// response, _ := json.Marshal(res_map) // convert map to json
		// w.Write(response)
		// w.Header().Set("Content-Type", "application/json") 
		// w.WriteHeader(http.StatusBadRequest)

		// w.Write([]byte ("invalid json data")) 
		// w.Write([]byte (err.Error()))


		return
	}

	// fmt.Println(stud)

	dbErr := stud.Create()
	if dbErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())

		// res_map := map[string]string{"error": dbErr.Error()}
		// response, _ := json.Marshal(res_map) // convert map to json
		// w.Write(response)
		// w.Header().Set("Content-Type", "application/json") 
		// w.WriteHeader(http.StatusBadRequest)
		// w.Write([]byte (dbErr.Error()))

		// w.Write([]byte ("error whille adding data to datatbase"))
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message":"student data added"})


	// res_map := map[string]string{"status": "student data added"}
	// response, _ := json.Marshal(res_map) // convert map to json
	// w.Write(response)
	// w.Header().Set("Content-Type", "application/json") 
	// w.WriteHeader(http.StatusBadRequest)
	// w.Write([]byte ("Student data added"))}

}
func getUserId(sid string) (int64) {
	ssid, _ := strconv.ParseInt(sid, 10, 64)
	return ssid 
}
// handler function to get values
func GetStud(w http.ResponseWriter, r *http.Request){

		// cookie verification
		if !VerifyCookie(w, r){
			return
		}
	sid := mux.Vars(r)["sid"]
	ssid := getUserId(sid)
	stud := model.Student{StdId:ssid} //creating an instance for
	// fmt.Println(stud)

	getErr:= stud.Read()
	if getErr != nil {
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)

	}
}

func UpdateStud(w http.ResponseWriter, r *http.Request){

	// cookie verification
	if !VerifyCookie(w, r){
		return
	}

	// getting id from parameter that we want to update
	old_sid := mux.Vars(r)["sid"]
	old_StdId := getUserId(old_sid)
	var stud  model.Student
	decoder :=	json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalide json body")
		return
	}
	updateErr := stud.Update(old_StdId)
	if updateErr != nil {
		switch updateErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)

	}
}

// delete func
func DeleteStud(w http.ResponseWriter, r *http.Request) {

		// cookie verification
		if !VerifyCookie(w, r){
			return
		}

	sid := mux.Vars(r)["sid"]
	StdId := getUserId(sid)

	s := model.Student{StdId: StdId}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	} 
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status":"deleted"})
}


func GetAllStudents(w http.ResponseWriter, r *http.Request) {

// cookie verification
if !VerifyCookie(w, r){
	return
}
	
	students, getErr := model.GetAllStudents()
if getErr != nil {
httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
return
}
httpResp.RespondWithJSON(w, http.StatusOK, students)

}