package httpResp

import (
	"encoding/json"
	"net/http"
)

func RespondWithError(w http.ResponseWriter, code int, message string) {
	RespondWithJSON(w, code, map[string]string{"error":message})
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}){
	// res_map := map[string]string{"error": dbErr.Error()}
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json") 
	w.WriteHeader(code)
	w.Write(response)
}